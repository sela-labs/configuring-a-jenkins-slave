# Configuring a Jenkins Slave
---

## Preparation

 - Ask the instructor for the server IP and credentials


## Instructions

 - Create a new folder to be used for the Jenkins slave:

```
$ sudo mkdir /home/jenkins
$ sudo chmod 777 /home/jenkins
```

 - Create a new node in the jenkins portal:

```
"Manage Jenkins" / "Manage Nodes" / "New Node"
```

![Image 1](images/image-01.png)


 - Set the server IP as the node name and select "Permanent Agent"
 
![Image 2](images/image-02.png)


 - Configure the slave with the details below:

```
number of executors: 5
Remote root directory: /home/jenkins
Labels: <your-server-ip>
Launch method: Launch agent agents via SSH
Host: <your-server-ip>
Credentials: sela
Host Key Verification Strategy: Non verifying Verification Strategy
```

![Image 3](images/image-03.png)


 - Click in "Refresh Status" and ensure that your node is correctly configured
 
![Image 4](images/image-04.png)
